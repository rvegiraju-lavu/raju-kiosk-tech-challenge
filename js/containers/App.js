/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StatusBar,
} from 'react-native';

import { Provider } from 'react-redux';
import store from '../store';


import HomeScreen from '../screens/homescreen/Homescreen'


const App: () => React$Node = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <Provider store = {store}>
            <HomeScreen/>
        </Provider>
      </SafeAreaView>
    </>
  );
};

export default App;
