
import { combineReducers } from 'redux';

import home from '../screens/homescreen/reducers/home';

export default combineReducers({
    home,
});