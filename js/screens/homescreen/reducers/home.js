
import * as actions from '../actions/home';

const initialState = {
  currentMenuGroups:[],
  currentCategories:[],
  activeMenuItems:[],
  isFetching: false,
  error: false,
  testingValue:10,
  selectedMenuID: "",
  selectedCategoryID: "",
};

export default function reducer(state = initialState, action) {

    switch (action.type) {
        case actions.MAKE_API_CALL:{
            console.log('MAKE_API_CALL');
            return{
                ...state, isFetching: true
            }
        }break;

        case actions.GOT_API_RESPONSE:{

            let tempCurrentMenuGroups   = action.response.data.MenuGroups;
            let tempCurrentCategories   = tempCurrentMenuGroups ? tempCurrentMenuGroups[0].categories : [];
            let tempActiveMenuItems     = tempCurrentCategories ? tempCurrentCategories[0].items : [];

            let tempSelectedMenuID      = tempCurrentMenuGroups ? tempCurrentMenuGroups[0].id : "";
            let tempSelectedCategoryID  = tempCurrentCategories ? tempCurrentCategories[0].id : "";

            return{
                ...state,
                isFetching:false,
                currentMenuGroups:tempCurrentMenuGroups,
                currentCategories:tempCurrentCategories,
                activeMenuItems:tempActiveMenuItems,
                selectedMenuID: tempSelectedMenuID,
                selectedCategoryID: tempSelectedCategoryID,
            }
        }break;
        case actions.MENU_GROUP_CHANGE:{
            let tempCategories          = state.currentMenuGroups.find((menu) => menu.id == action.selectedMenuID).categories;
            let tempactiveMenuItems     = tempCategories ? tempCategories[0].items : [];
            let tempSelectedMenuID      = action.selectedMenuID;
            let tempSelectedCategoryID  = tempCategories ? tempCategories[0].id : "";
            return{
                ...state,
                currentCategories:tempCategories,
                activeMenuItems:tempactiveMenuItems,
                selectedMenuID: tempSelectedMenuID,
                selectedCategoryID: tempSelectedCategoryID,
            }
        }break;
        case actions.CATEGORY_CHANGED:{
            console.log('CATEGORY_CHANGED');
            let tempMenuItems           = state.currentCategories.find((category) => category.id == action.selectedCategory).items;
            let tempSelectedCategoryID  = action.selectedCategory;
            return{
                ...state,
                activeMenuItems:tempMenuItems,
                selectedCategoryID: tempSelectedCategoryID,
            }
        }break;
        default:{
            return state;
        }break;
    }
}

