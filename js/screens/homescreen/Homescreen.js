
import React from 'react';
import {
    View,
    Text,
    FlatList,
    TouchableOpacity,
    ActivityIndicator,
    TextInput,
} from 'react-native';
import { connect } from 'react-redux';

import { makeAPICall, menuGroupChanged, categoryChanged } from './actions/home';
import Styles from './homestyles';
import MenuItem from '../MenuItem';


export class HomeScreen extends React.Component {

    constructor(props) {
        super(props);
        this.makeAPICallButtonReference = this.makeAPICall.bind(this);
    }

    componentDidMount() {
        this.makeAPICall();
    }

    makeAPICall() {
        this.props.makeAPICall('api/menu')
    }

    menuSelected(selectedMenu) {
        console.log(`Selected menu id is ${selectedMenu.id}`);
        if (this.props.selectedMenuID != selectedMenu.id) {
            this.props.updateMenu(selectedMenu.id);
        }
    }

    categorySelected(selectedCategory) {
        console.log(`category selected with ${selectedCategory.id}`);
        if (this.props.selectedCategoryID != selectedCategory.id) {
            this.props.updateCategory(selectedCategory.id);
        }
    }
    renderMenuGroupItem(menuCategory) {
        return (
            <TouchableOpacity
                style={Styles.menuItem}
                onPress={() => this.menuSelected(menuCategory)}>
                <View style={{ paddingLeft: 15, paddingRight: 15 }}>
                    <Text
                        style={menuCategory.id == this.props.selectedMenuID ?
                            Styles.menuGroupTextSelected
                            :
                            Styles.menuGroupTextNormal}>
                        {menuCategory.name.toUpperCase()}
                    </Text>
                </View>
            </TouchableOpacity>
        )
    }

    renderCategory(category) {
        return (
            <TouchableOpacity
                style={Styles.categoryItem}
                onPress={() => this.categorySelected(category)} >
                <View style={{ paddingLeft: 10, paddingRight: 10 }}>
                    {
                        category.id === this.props.selectedCategoryID ?
                            <View styles={Styles.categoryTextSelected}>
                                <Text style={Styles.categoryTextSelected}>
                                    {category.name}
                                </Text>
                                <Text style={Styles.categorySelectIndicator} />
                            </View>
                            :
                            <Text style={Styles.categoryTextNormal}>
                                {category.name}
                            </Text>
                    }
                </View>
            </TouchableOpacity>
        )
    }

    renderActiveMenuItem(menuItem) {
        return (
            <View>
                <MenuItem
                    item={menuItem}
                />
            </View>
        )
    }

    render() {
        if (this.props.isFetching) {
            return (
                <View style={Styles.activity}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            )
        } else {
            return (
                <View style={Styles.parentView}>
                    {this.props.currentMenuGroups &&
                        <View
                            style={Styles.menuGroup}
                        >
                            <FlatList
                                data={this.props.currentMenuGroups}
                                renderItem={({ item }) =>
                                    this.renderMenuGroupItem(item)
                                }
                                keyExtractor={(item) => item.id.toString()}
                                horizontal={true}
                                extraData={this.props.currentMenuGroups}
                            />
                        </View>
                    }
                    {this.props.currentCategories &&
                        <View
                            style={Styles.category}
                        >
                            <FlatList
                                data={this.props.currentCategories}
                                renderItem={({ item }) =>
                                    this.renderCategory(item)
                                }
                                keyExtractor={(item) => item.id.toString()}
                                horizontal={true}
                                extraData={this.props.currentCategories}
                            />
                        </View>
                    }

                    {this.props.activeMenuItems &&
                        <View
                            style={Styles.activeMenu}
                        >
                            <FlatList
                                data={this.props.activeMenuItems}
                                renderItem={({ item }) =>
                                    this.renderActiveMenuItem(item)
                                }
                                numColumns={2}
                                keyExtractor={(item) => item.id.toString()}
                                horizontal={false}
                                extraData={this.props.activeMenuItems}
                            />
                        </View>
                    }
                </View>
            );
        }
    }
}

const mapStateToProps = (state) => ({
    isFetching: state.home.isFetching,
    currentMenuGroups: state.home.currentMenuGroups,
    currentCategories: state.home.currentCategories,
    activeMenuItems: state.home.activeMenuItems,
    selectedMenuID: state.home.selectedMenuID,
    selectedCategoryID: state.home.selectedCategoryID
});

const mapDispatchToProps = (dispatch) => ({
    makeAPICall: (path) => dispatch(makeAPICall(path)),
    updateMenu: (selectedMenuID) => dispatch(menuGroupChanged(selectedMenuID)),
    updateCategory: (selectedCategory) => dispatch(categoryChanged(selectedCategory))
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);