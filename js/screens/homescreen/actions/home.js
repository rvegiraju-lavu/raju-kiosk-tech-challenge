
import { BASE_URL } from '../../../config';

export const MAKE_API_CALL = 'MAKE_API_CALL';
export const GOT_API_RESPONSE = 'GOP_API_RESPONSE';
export const MENU_GROUP_CHANGE = 'MENU_GROUP_CHANGE';
export const CATEGORY_CHANGED = 'CATEGORY_CHANGED';
export const ERROR = 'ERROR';


export function makeAPICall(path) {

    console.log(`The path is ${BASE_URL}`);
    console.log(`Path is ${path}`);

    return dispatch =>{
        fetch(`${BASE_URL}/api/menu`)
            .then(res => res.json())
            .then(res => {
                console.log("Response Order Status data");
                console.log(res);
                console.log('----------');
                dispatch(gotResponseFromServer(res));
            })
            .catch(error => {
                console.log(error);
            });
            dispatch(apiCallInitiated());
    }

        
            
}

export function apiCallInitiated(){
    return{
        type:MAKE_API_CALL
    }
}

export function gotResponseFromServer(response) {
    console.log('passed to gotResponseFromServer ')
    console.log(`${response}`);
    return {
        type: GOT_API_RESPONSE,
        response
    }
}

export function errorOccured() {
    return {
        type: ERROR
    }
}

export function menuGroupChanged(selectedMenuID) {
    console.log('passed to menuGroupChanged ')
    return {
        type: MENU_GROUP_CHANGE,
        selectedMenuID
    }
}

export function categoryChanged(selectedCategory) {
    console.log('passed to categoryChanged ')
    return{
        type: CATEGORY_CHANGED,
        selectedCategory
    }
    
}