import {
    StyleSheet
} from 'react-native';

const style = StyleSheet.create({
    parentView: {
        flex: 1,
        flexDirection:'column',
    },
    activity: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    menuGroupTextSelected: {
        color: 'white',
        fontSize: 25,
    },
    menuGroupTextNormal: {
        color: 'rgba(181,173,195,1)',
        fontSize: 25,
    },
    categoryTextSelected: {
        color: 'white',
        fontSize: 20,
    },
    categoryTextNormal: {
        color: 'rgba(181,173,195,1)',
        fontSize: 20,
    },
    menuGroup: {
        width: '100%',
        height: 60,
        backgroundColor: 'rgba(67,40,118,1)',
    },

    menuItem: {
        flex: 1,
        justifyContent: 'center',
    },

    category: {
        width: '100%',
        height:60,
        backgroundColor: 'rgba(51,31,82,1)',
    },
    categoryItem: {
        flex: 1,
        justifyContent: 'center',
    },
    activeMenu: {
        margin:10,
        backgroundColor: 'white',
        width: '100%',
        height: 600,
        justifyContent: 'center',
    },
    categorySelectIndicator: {
        backgroundColor: 'white',
        height: 3,
        alignSelf: 'stretch',
    }
});

export default style;