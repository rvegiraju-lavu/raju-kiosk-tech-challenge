
import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    ScrollView
} from 'react-native';

import LoadableImage from './LoadingImage';

const MenuItem = ({ item }) => {
    console.log(`Item name is ${item.name}`);
    return (
        <View style={styles.holder}>
            <View style={[styles.container, { opacity: 1 }]}>
                <View style={styles.itemContent}>
                    <Text style={styles.name} numberOfLines={3} multiline={true}>
                        {item.name}
                    </Text>
                    <Text style={styles.desc} numberOfLines={2} multiline={true}>
                        {item.description}
                    </Text>
                    <View style={styles.space} />
                    <Text style={styles.price}>
                        ${parseFloat(item.price).toFixed(2)}
                    </Text>
                </View>
                <Image
                   style={styles.image}
                   source={{
                       uri: `https://via.placeholder.com/300.png?text=${item.name}`,
                   }}
               />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 160,
        width: 316,
        padding: 16,
        flexDirection: 'row',
    },
    scrollContainer: {
        borderRadius: 12,
        height: 160,
        width: 316,
    },
    holder: {
        marginBottom: 16,
        borderRadius: 12,
        marginRight: 16,
        backgroundColor: '#fff',
        shadowOffset: { width: 0, height: 4 },
        shadowRadius: 12,
        shadowColor: 'rgba(32, 24, 61, 0.08)',
        shadowOpacity: 1,
    },
    image: {
        backgroundColor:'gray',
        width: 120,
        height: 160,
        marginTop: -16,
        marginRight: -16,
        borderTopRightRadius: 12,
        borderBottomRightRadius: 12,
    },
    itemContent: {
        flex: 1,
    },
    space: {
        flex: 1,
    },
    name: {
        fontSize: 17,
        color: '#23163f',
    },
    desc: {
        fontSize: 17,
        color: 'rgba(115, 111, 122, 0.9)',
        marginTop: 8,
    },
    price: {
        fontSize: 14,
        color: '#23163f',
    },
});
export default MenuItem;