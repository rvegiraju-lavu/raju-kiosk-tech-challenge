import React, { Component } from 'react';
import { StyleSheet, View, Image, ActivityIndicator } from 'react-native';

export default class LoadableImage extends Component {
  state = {
    loading: true
  }

  render() {
    const { url } = this.props;
    console.log(`url is ${url}`);

    return (
      <View style={styles.container}>
        <Image
          onLoadStart = { ()=> console.log('Image loading started')}
          onLoadEnd={ () => console.log('Image loaded success')}
          source={{ uri: url }}
        />
        <ActivityIndicator
          style={styles.activityIndicator}
          animating={this.state.loading}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  activityIndicator: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  }
})